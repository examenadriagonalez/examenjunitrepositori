package org.iesam.adriangonzalez.test;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.iesam.adriangonzalez.Product;
import org.iesam.adriangonzalez.ProductNotFoundException;
import org.iesam.adriangonzalez.ShoppingCartAdrianGonzalez;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;
//public class ShoppingCartTest {
//
//	@Test
//	public void test() {
//		fail("Not yet implemented");
//	}
//
//}


public class ShoppingCartTest  {

    private ShoppingCartAdrianGonzalez _bookCart;
    private Product _defaultBook;

    /**
     * Sets up the test fixture.
     * Called before every test case method.
     */
    @Before
    public void prepararTest() {

        _bookCart = new ShoppingCartAdrianGonzalez();

        _defaultBook = new Product("Extreme Programming", 23.95);
        _bookCart.addItem(_defaultBook);
    }

    /**
     * Tears down the test fixture.
     * Called after every test case method.
     */
    @After
    public void limpiarTest() {
        _bookCart = null;
    }
    
    @Test
    public void compruebaVacío() {
    	_bookCart.empty();
    	assertTrue(_bookCart.isEmpty());
    }
    
    @Test
    public void añadirProducto() {
    	Product _defaultBook2 = new Product("Programming", 1.05);
    	_bookCart.addItem(_defaultBook2);
    	assertEquals(25, _bookCart.getBalance(),0);
    	
    	assertEquals(2,_bookCart.getItemCount(),0);
    }
    
    @Test(expected = ProductNotFoundException.class)
    public void	productoNoExistente() throws ProductNotFoundException {
    	Product _defaultBook2 = new Product("Programming", 1.05);
    	_bookCart.removeItem(_defaultBook2);
    	fail("Se esperaba excepcion ProductNotFoundException");
    }
    
    public void quitarProducto() throws ProductNotFoundException {
    	_bookCart.removeItem(_defaultBook);
    	
    	assertEquals(0,_bookCart.getItemCount(),0);
    	assertEquals(0,_bookCart.getBalance(),0);
    }
    
    
    
    
    
    
    
    
    
    
    
    
}